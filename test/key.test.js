var assert = require('assert');
var Key = require('../src/js/key');

describe('Key', function () {
  describe('#isEqual()', function () {
    it('check if is equal with equivalent TRUE', function () {
      var KeyA = new Key({code: 'KeyA', key: 'a'});
      var KeyABig = new Key({code: 'KeyA', key: 'A'});
      var KeyADuplicate = new Key({code: 'KeyA', key: 'a'});
      var KeyB = new Key({code: 'KeyB', key: 'b'});
      var Control = new Key({code: 'Control', key: 'Control'});
      var ControlRight = new Key({code: 'ControlRight', key: 'ControlRight'});
      var ControlLeft = new Key({code: 'ControlLeft', key: 'ControlLeft'});
      var ControlLeftDuplicate = new Key({code: 'ControlLeft', key: 'ControlLeft'});

      assert(KeyA.isEqual(KeyADuplicate), 'KeyA != KeyADuplicate');
      assert(KeyA.isEqual(KeyABig), 'KeyA != KeyABig');
      assert(!KeyA.isEqual(KeyB), 'KeyA == KeyB');
      assert(Control.isEqual(ControlRight), 'Control != ControlRight');
      assert(ControlLeft.isEqual(ControlRight), 'ControlLeft != ControlRight');
      assert(ControlLeft.isEqual(Control), 'ControlLeft != Control');
      assert(ControlLeft.isEqual(ControlLeftDuplicate), 'ControlLeft != ControlLeftDuplicate');
    });

    it('check if is equal with equivalent FALSE', function () {
      var KeyA = new Key({code: 'KeyA', key: 'a'}, false);
      var KeyABig = new Key({code: 'KeyA', key: 'A'}, false);
      var KeyADuplicate = new Key({code: 'KeyA', key: 'a'}, false);
      var KeyB = new Key({code: 'KeyB', key: 'b'}, false);
      var Control = new Key({code: 'Control', key: 'Control'}, false);
      var ControlLeft = new Key({code: 'ControlLeft', key: 'ControlLeft'}, false);
      var ControlRight = new Key({code: 'ControlRight', key: 'ControlRight'}, false);
      var ControlRightDuplicate = new Key({code: 'ControlRight', key: 'ControlRight'}, false);

      assert(KeyA.isEqual(KeyADuplicate), 'KeyA != KeyADuplicate');
      assert(KeyA.isEqual(KeyABig), 'KeyA != KeyABig');
      assert(!KeyA.isEqual(KeyB), 'KeyA == KeyB');
      assert(!Control.isEqual(ControlRight), 'Control == ControlRight');
      assert(!ControlLeft.isEqual(ControlRight), 'ControlLeft == ControlRight');
      assert(ControlRight.isEqual(ControlRightDuplicate), 'ControlRight != ControlRightDuplicate');
    });
  });
});
