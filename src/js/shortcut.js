(function (id, dependency, root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(dependency, factory);
  } else if (typeof exports === 'object') {
    module.exports = factory();
  } else {
    root[id] = factory();
  }
})('Shortcut', [], this, function () {
  'use strict';

  function Shortcut(strict) {
    this.strict = Boolean(strict);
    this.keys = [];
  }

  Shortcut.prototype.hasKey = function (key) {
    return Boolean(this.keys.filter(function (keyElm) {
      return keyElm === key || keyElm.isEqual(key);
    }, this).length);
  };

  Shortcut.prototype.addKey = function (key) {
    if (!this.hasKey(key)) {
      this.keys.push(key);
    }
  };

  Shortcut.prototype.removeKey = function (key) {
    this.keys = this.keys.filter(function (keyElm) {
      return !(keyElm === key || keyElm.isEqual(key));
    }, this);
  };

  Shortcut.prototype.clear = function () {
    this.keys = [];
  };

  Shortcut.prototype.conflict = function (keys) {
    for (var i = 0; i < keys.length; i++) {
      var foundIndex = this.keys.indexOf(keys[i]);

      if (foundIndex !== -1) {
        // check if keys exist in shortcut
      }
    }

    return false;
  };

  Shortcut.prototype.toString = function () {
    return this.keys.map(function (key) {
      return key.toString(this.strict);
    }.bind(this)).join(' + ');
  };

  return Shortcut;
});
