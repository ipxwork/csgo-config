(function (id, dependency, root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(dependency, factory);
  } else if (typeof exports === 'object') {
    module.exports = factory();
  } else {
    root[id] = factory();
  }
})('Key', [], this, function () {
  'use strict';

  /**
   * Object that describe a keyboard key
   * {@link https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code}
   * @typedef {Object} KeyOptions
   * @property {string} code - Key code
   * @property {string} key - Key value
   */

  /**
   * Info: http://steamcommunity.com/sharedfiles/filedetails/?id=203603867
   * @constructor Key
   * @param {KeyOptions|KeyboardEvent} keyOptions Object that describe key or KeyboardEvent
   * @param {boolean} [equivalent=true] Indicate that key behavior equivalent
   */
  function Key(keyOptions, equivalent) {
    if ((typeof KeyboardEvent !== 'undefined' && keyOptions instanceof KeyboardEvent) || (keyOptions.hasOwnProperty('code') && keyOptions.hasOwnProperty('key'))) {
      var coldKey = keyOptions.code.match(/^(Control|Alt|Shift)(Left|Right|)$/);

      this.equivalent = typeof equivalent !== 'undefined' ? Boolean(equivalent) : true;
      this.key = keyOptions.key || keyOptions.keyCode;

      if (this.equivalent && coldKey) {
        this.code = coldKey[1];
      } else {
        this.code = keyOptions.code || keyOptions.charCode;
      }

      this.bindKey = this.getBindKey(this);
    } else {
      throw Error('keyOptions need to have code and key property');
    }
  }

  Key.prototype.isEqual = function (key) {
    if (this.equivalent) {
      var coldKey = key.code.match(/^(Control|Alt|Shift)(Left|Right|)$/);

      if (coldKey) {
        return this.code === coldKey[1];
      }
    }

    return this.code === key.code;
  };

  Key.prototype.toString = function (useKey) {
    if (useKey) {
      var key = this.code;

      if (key === ' ' || key === '') {
        key = this.code;
      }

      return key.toUpperCase();
    }

    return this.code;
  };

  Key.prototype.valueOf = function () {
    return this.key;
  };

  Key.prototype.getBindKey = function (key) {
    var direction = {
      Up: 'UP',
      Down: 'DN',
      Left: '',
      Right: 'R',
      '': ''
    };
    var numPad = {
      Divide: 'SLASH',
      Multiply: 'MULTIPLY',
      Subtract: 'MINUS',
      Add: 'PLUS',
      Enter: 'ENTER',
      Decimal: 'DEL',
      0: 'INS',
      1: 'END',
      2: 'DOWNARROW',
      3: 'PGDN',
      4: 'LEFTARROW',
      5: '5',
      6: 'RIGHTARROW',
      7: 'HOME',
      8: 'UPARROW',
      9: 'PGUP'
    };
    var arrowKey = key.code.match(/^(Arrow)(Up|Down|Left|Right)$/);
    var coldKey = key.code.match(/^(Control|Alt|Shift)(Left|Right|)$/);
    var pageKey = key.code.match(/^(Page)(Up|Down)$/);
    var alphaKey = key.code.match(/^(Key)([A-Z])$/);
    var numKey = key.code.match(/^(Numpad)(.+)$/);

    if (arrowKey) {
      return (arrowKey[2] + arrowKey[1]).toUpperCase();
    } else if (coldKey) {
      return (direction[coldKey[2]] + (coldKey[1] === 'Control' ? 'Ctrl' : coldKey[1])).toUpperCase();
    } else if (pageKey) {
      return 'PG' + direction[pageKey[2]];
    } else if (alphaKey) {
      return alphaKey[2].toUpperCase();
    } else if (numKey) {
      return 'KP_' + numPad[numKey[2]];
    } else if (key.code === 'Insert') {
      return 'INS';
    } else if (key.code === 'Delete') {
      return 'DEL';
    } else if (key.code === 'Semicolon') {
      return 'SEMICOLON';
    } else if (['Escape', 'ContextMenu', 'ScrollLock'].indexOf(key.code) !== -1) {
      return null;
    }

    return key.code.toUpperCase();
  };

  return Key;
});
