(function (id, dependency, root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(dependency, factory);
  } else if (typeof exports === 'object') {
    module.exports = factory();
  } else {
    root[id] = factory();
  }
})('Keyboard', ['Shortcut'], this, function (Shortcut) {
  'use strict';

  function Keyboard(strict) {
    this.strict = Boolean(strict);
    this.keys = {};
    this.shortcuts = {};
    this.selectedKeys = [];
  }

  Keyboard.prototype.hasKey = function (key) {
    return this.keys.hasOwnProperty(typeof key === 'string' ? key : key.code);
  };

  Keyboard.prototype.addKey = function (key) {
    if (!this.hasKey(key)) {
      this.keys[key.code] = key;

      return true;
    }

    return false;
  };

  Keyboard.prototype.removeKey = function (key) {
    if (this.hasKey(key)) {
      delete this.keys[key.code];

      return true;
    }

    return false;
  };

  Keyboard.prototype.getKey = function (key) {
    var code = typeof key === 'string' ? key : key.code;

    if (this.hasKey(code)) {
      return this.keys[code];
    }

    return null;
  };

  Keyboard.prototype.isSelectedKey = function (key, getIndex) {
    var returnValue = -1;

    this.selectedKeys.forEach(function (keyObj, index) {
      if (typeof key === 'string') {
        if (keyObj.code === key) {
          returnValue = index;
        }
      } else if (keyObj.isEqual(key)) {
        returnValue = index;
      }
    });

    return getIndex ? returnValue : returnValue !== -1;
  };

  Keyboard.prototype.selectKey = function (key) {
    if (!this.isSelectedKey(key)) {
      this.selectedKeys.push(key);

      return true;
    }

    return false;
  };

  Keyboard.prototype.unSelectKey = function (key) {
    if (this.isSelectedKey(key)) {
      this.selectedKeys.splice(this.isSelectedKey(key, true), 1);

      return true;
    }

    return false;
  };

  Keyboard.prototype.getSelected = function () {
    return this.selectedKeys;
  };

  Keyboard.prototype.makeShortcut = function () {
    if (this.getShortcuts(this.selectedKeys).length) {
      return false;
    }
    var shortCut = new Shortcut();

    for (var i = 0; i < this.selectedKeys.length; i++) {
      var code = this.selectedKeys[i];
      var key = this.hasKey(code) ? this.getKey(code) : this.selectedKeys[i];

      this.addKey(key);
      shortCut.addKey(key);
      if (!Array.isArray(this.shortcuts[code])) {
        this.shortcuts[code] = [];
      }
      this.shortcuts[code].push(shortCut);
    }

    this.selectedKeys = [];

    return true;
  };

  Keyboard.prototype.getShortcuts = function (keys) {
    var shortcuts = [];

    for (var i = 0; i < keys.length; i++) {
      var code = keys[i].code;

      if (this.shortcuts.hasOwnProperty(code)) {
        shortcuts = this.shortcuts[code].slice();
        break;
      }
    }

    shortcuts = shortcuts.filter(function (shortcut) {
      return shortcut.conflict(keys);
    });

    return shortcuts;
  };

  return Keyboard;
});
