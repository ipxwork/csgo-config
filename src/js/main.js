'use strict';

require.config({
  baseUrl: 'js',
  paths: {
    Key: 'key',
    Shortcut: 'shortcut',
    Keyboard: 'keyboard'
  }
});

require(['Key', 'Shortcut', 'Keyboard'], function (Key, Shortcut, Keyboard) {
  /**
   * @type Keyboard
   */
  var keyboard = new Keyboard();

  document.addEventListener('keydown', function (event) {
    var key = new Key(event);

    if (event.code === 'Escape') {
      console.log(keyboard.getShortcuts(keyboard.selectedKeys).join(', '));
      console.log('shortcut added', keyboard.makeShortcut());
      console.info('all shortcuts:', keyboard.shortcuts);
    } else if (keyboard.isSelectedKey(key)) {
      keyboard.unSelectKey(key);
    } else {
      keyboard.selectKey(key);
    }
    console.info('key pressed:', key);
    console.info('selected keys', keyboard.selectedKeys);
  });
});
